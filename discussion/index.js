const express = require('express');
const { addListener } = require('nodemon');
const application = express();
const port = 4000;
application.listen(port,()=> console.log(`Server is running on port ${port}`))